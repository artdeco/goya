import { c } from 'erte'

/**
 * @type {_goya.goya}
 */
export default async function goya(config = {}) {
  const {
    shouldRun = true,
    text = '',
  } = config
  if (!shouldRun) return ''
  console.log('goya called with %s', c(text, 'yellow'))
  return text
}

/**
 * @suppress {nonStandardJsDocs}
 * @typedef {import('../types').goya} _goya.goya
 */
