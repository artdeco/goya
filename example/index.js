import goya from '../src'

(async () => {
  const res = await goya({
    text: 'example',
  })
  console.log(res)
})()