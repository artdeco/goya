## API

The package is available by importing its default function:

```js
import goya from 'goya'
```

%~%

<typedef method="goya">types/api.xml</typedef>

<typedef>types/index.xml</typedef>

%EXAMPLE: example, ../src => goya%
%FORK example%

%~%