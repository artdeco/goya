const { _goya } = require('./goya')

/**
 * GoyaScript (GS) RunTime In Chrome.
 * @param {!_goya.Config} config Options for the program.
 * @param {boolean} [config.shouldRun=true] A boolean option. Default `true`.
 * @param {string} [config.text] A text to return.
 * @return {Promise<string>}
 */
function goya(config) {
  return _goya(config)
}

module.exports = goya

/* typal types/index.xml namespace */
/**
 * @typedef {_goya.Config} Config `＠record` Options for the program.
 * @typedef {Object} _goya.Config `＠record` Options for the program.
 * @prop {boolean} [shouldRun=true] A boolean option. Default `true`.
 * @prop {string} [text] A text to return.
 */
