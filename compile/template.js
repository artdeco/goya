const { _goya } = require('./goya')

/**
 * @methodType {_goya.goya}
 */
function goya(config) {
  return _goya(config)
}

module.exports = goya

/* typal types/index.xml namespace */
