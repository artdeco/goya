export {}

/* typal types/api.xml namespace */
/**
 * @typedef {_goya.goya} goya GoyaScript (GS) RunTime In Chrome.
 * @typedef {(config: !_goya.Config) => string} _goya.goya GoyaScript (GS) RunTime In Chrome.
 */

/**
 * @typedef {import('..').Config} _goya.Config
 */