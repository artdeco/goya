/**
 * @fileoverview
 * @externs
 */

/* typal types/index.xml externs */
/** @const */
var _goya = {}
/**
 * Options for the program.
 * @record
 */
_goya.Config
/**
 * A boolean option. Default `true`.
 * @type {boolean|undefined}
 */
_goya.Config.prototype.shouldRun
/**
 * A text to return.
 * @type {string|undefined}
 */
_goya.Config.prototype.text

/* typal types/api.xml externs */
/**
 * GoyaScript (GS) RunTime In Chrome.
 * @typedef {function(!_goya.Config): !Promise<string>}
 */
_goya.goya
